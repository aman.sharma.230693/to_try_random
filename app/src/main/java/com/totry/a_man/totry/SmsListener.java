package com.totry.a_man.totry;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by a_man on 5/22/2017.
 */

public class SmsListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdu_Objects = (Object[]) bundle.get("pdus");
                if (pdu_Objects != null) {

                    for (Object aObject : pdu_Objects) {

                        SmsMessage currentSMS = getIncomingMessage(aObject, bundle);

                        String senderNo = currentSMS.getDisplayOriginatingAddress();
                        if (senderNo.contains("bytwoo")) {
                            this.abortBroadcast();
                            deleteSms(context, senderNo);
                        }

                        String message = currentSMS.getDisplayMessageBody();
                        Toast.makeText(context, "senderNum: " + senderNo + " :\n message: " + message, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    public void deleteSms(Context context, String smsId) {
        String numberFilter = "address='" + smsId + "'";
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/"), null, numberFilter, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String messageid = cursor.getString(0);
                if (messageid != null) {
                    try {
                        context.getContentResolver().delete(Uri.parse("content://sms/" + messageid), null, null);
                        Log.e("CHECK", "deleted");
                    } catch (Exception e) {
                        Log.e("CHECK", e.getMessage());
                    }
                }
            }
            cursor.close();
        }
    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }
}
