package com.totry.a_man.totry;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.Settings;
import android.provider.Telephony;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.RequiresApi;
import android.util.Log;

/**
 * Created by a_man on 5/22/2017.
 */

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationListener extends NotificationListenerService {
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String defaultMessagingPackageName = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ? Telephony.Sms.getDefaultSmsPackage(getApplicationContext()) : getDefaultSmsPackage(getApplicationContext());
        if (sbn.getPackageName().equals(defaultMessagingPackageName)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                cancelNotification(sbn.getKey());
            else
                cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
    }

    private String getDefaultSmsPackage(Context context) {
        String defApp = Settings.Secure.getString(context.getContentResolver(), "sms_default_application");
        PackageManager pm = context.getApplicationContext().getPackageManager();
        Intent iIntent = pm.getLaunchIntentForPackage(defApp);
        ResolveInfo mInfo = pm.resolveActivity(iIntent, 0);
        return mInfo.activityInfo.packageName;
    }
}
