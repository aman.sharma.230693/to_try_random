package com.totry.a_man.totry;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.sax.RootElement;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private final String COMMAND_FLIGHT_MODE_1 = "settings put global airplane_mode_on";
    private final String COMMAND_FLIGHT_MODE_2 = "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //webView("http://tachemr.com:81/index.php?r=survey/index&sid=148762&lang=en");
        //fileChecks();
        //enableDataDisableWifi();
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
//            dial();
//        else
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 123);
        //dualSimInfo();

        Toast.makeText(MainActivity.this, "Rooted " + (checkRootMethod2() || checkRootMethod3() ? "YES" : "NO"), Toast.LENGTH_LONG).show();

        findViewById(R.id.apmStatus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Airplane mode is " + (isAirplaneModeOn(MainActivity.this) ? "ON" : "OFF"), Toast.LENGTH_SHORT).show();
            }
        });
        findViewById(R.id.apmTurnOff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAirplaneModeOn(MainActivity.this)) {
                    setFlightMode(MainActivity.this, false);
                }
            }
        });
        findViewById(R.id.apmTurnOn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAirplaneModeOn(MainActivity.this)) {
                    setFlightMode(MainActivity.this, true);
                }
            }
        });
    }

    public boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        else
            return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    public void setFlightMode(Context context, boolean onOff) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards.
            if (checkRootMethod2() || checkRootMethod3()) {
                int enabled = onOff ? 1 : 0;
                // Set Airplane / Flight mode using su commands.
                String command = COMMAND_FLIGHT_MODE_1 + " " + enabled;
                executeCommandWithoutWait(context, "-c", command);
                command = COMMAND_FLIGHT_MODE_2 + " " + enabled;
                executeCommandWithoutWait(context, "-c", command);
            } else {
                try {
                    // No root permission, just show Airplane / Flight mode setting screen.
                    Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("CHECK", "Setting screen not found due to: " + e.fillInStackTrace());
                }
            }
        } else {
            // API 16 and earlier.
            Settings.System.putInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, onOff ? 1 : 0);
            Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intent.putExtra("state", onOff);
            sendBroadcast(intent);
        }
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths)
            if (new File(path).exists()) return true;
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return in.readLine() != null;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    // executes a command on the system
    private boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

    private void executeCommandWithoutWait(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i = 0; i < 3; i++) {
            // "su" command executed successfully.
            if (success) {
                // Stop executing alternative su commands below.
                break;
            }
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // execute command
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                Log.e("CHECK", "su command has failed due to: " + e.fillInStackTrace());
            }
        }

        //boolean isEnabled = Settings.System.getInt(this.getContentResolver(),Settings.System.AIRPLANE_MODE_ON, 0) == 1;
//        Settings.Global.putInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, Integer.valueOf(command.substring(command.length() - 1, command.length())));

    }

    private void checkNotificationAccess() {
        boolean granted = false;
        for (String packageName : NotificationManagerCompat.getEnabledListenerPackages(this))
            if (packageName.equals(getPackageName())) {
                granted = true;
                break;
            }

        if (!granted)
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    private void dualSimInfo() {
        TelephonyInfo mTelephonyMgr = TelephonyInfo.getInstance(this);
        TelephonyManager localTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        String str1 = mTelephonyMgr.getImsiSIM1();
        String str2 = mTelephonyMgr.getImsiSIM2();

        if (Build.VERSION.SDK_INT >= 23) {
            str1 = localTelephonyManager.getDeviceId(0);
            str2 = localTelephonyManager.getDeviceId(1);
            Log.e("CHECK", "Single or Dula Sim " + localTelephonyManager.getPhoneCount());
            Log.e("CHECK", "Default device ID " + localTelephonyManager.getDeviceId());
            Log.e("CHECK", "Single 1 " + localTelephonyManager.getDeviceId(0));
            Log.e("CHECK", "Single 2 " + localTelephonyManager.getDeviceId(1));
        }
        Log.e("CHECK", "simOperatorName : " + mTelephonyMgr.getOutput(this, "SimOperatorName", 0));
        Log.e("CHECK", "simOperatorName : " + mTelephonyMgr.getOutput(this, "SimOperatorName", 1));
        Log.e("CHECK", "SimSerialNumber : " + mTelephonyMgr.getOpByReflection(localTelephonyManager, "getSimSerialNumber", 1, true));
        Log.e("CHECK", "SimSerialNumber : " + mTelephonyMgr.getOpByReflection(localTelephonyManager, "getSimSerialNumber", 2, true));

        String imsi = localTelephonyManager.getSubscriberId();
        String imei = localTelephonyManager.getDeviceId();
        String simno = localTelephonyManager.getSimSerialNumber();
        Log.e("CHECK", "DefaultSimSerialNumber : " + simno);

//        Intent simIntent = new Intent("android.intent.action.SMS_DEFAULT_SIM");
//        simIntent.putExtra("simid", simno);
//        sendBroadcast(simIntent);
    }

    private void dial() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", "*199*2#", null));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void enableDataDisableWifi() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);

        setMobileDataEnabled(this, true);
    }

    private void setMobileDataEnabled(Context paramContext, boolean paramBoolean) {
        ConnectivityManager localConnectivityManager = (ConnectivityManager) paramContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Field localField = Class.forName(localConnectivityManager.getClass().getName()).getDeclaredField("mService");
            localField.setAccessible(true);

            Object localObject = localField.get(localConnectivityManager);
            Class localClass = Class.forName(localObject.getClass().getName());

            Method localMethod = localClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            localMethod.setAccessible(true);
            localMethod.invoke(localObject, paramBoolean);
            return;
        } catch (ClassNotFoundException localClassNotFoundException) {
            localClassNotFoundException.printStackTrace();
            return;
        } catch (InvocationTargetException localInvocationTargetException) {
            localInvocationTargetException.printStackTrace();
            return;
        } catch (NoSuchMethodException localNoSuchMethodException) {
            localNoSuchMethodException.printStackTrace();
            return;
        } catch (IllegalAccessException localIllegalAccessException) {
            localIllegalAccessException.printStackTrace();
            return;
        } catch (NoSuchFieldException localNoSuchFieldException) {
            localNoSuchFieldException.printStackTrace();
        }
    }

    private void fileChecks() {
        File fileSure = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/", "VID_20170326_082317.mp4");

        File file = new File(Environment.getExternalStorageDirectory(), "/Oyapp/Video/.sent/");
        boolean dirExists = file.exists();
        if (!dirExists)
            dirExists = file.mkdirs();
        if (dirExists)
            try {
                file = new File(Environment.getExternalStorageDirectory() + "/Oyapp/Video/.sent/", Uri.fromFile(fileSure).getLastPathSegment());
                boolean fileExists = file.exists();
                if (!fileExists)
                    fileExists = file.createNewFile();
                if (fileExists && file.length() == 0) {
                    Log.e("CHECK", Uri.fromFile(file).getLastPathSegment());
                    copyFile(fileSure, file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void copyFile(File src, final File dst) {
        AsyncTask<File, Void, Void> task = new AsyncTask<File, Void, Void>() {
            @Override
            protected Void doInBackground(File... params) {
                try {
                    FileInputStream inStream = new FileInputStream(params[0]);
                    FileOutputStream outStream = new FileOutputStream(params[1]);
                    FileChannel inChannel = inStream.getChannel();
                    FileChannel outChannel = outStream.getChannel();
                    inChannel.transferTo(0, inChannel.size(), outChannel);
                    inStream.close();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.e("CHECK", "DONE! " + dst.length());
            }
        };
        File[] arr = {src, dst};
        task.execute(arr);
    }

    private void webView(String url) {
//        WebView webView = (WebView) findViewById(R.id.webview);
//        WebSettings settings = webView.getSettings();
//        settings.setJavaScriptEnabled(true);
//        settings.setAppCacheEnabled(true);
//        settings.setBuiltInZoomControls(true);
//        settings.setPluginState(WebSettings.PluginState.ON);
//        webView.setWebChromeClient(new WebChromeClient());
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.d("CHECK", url);
//                return false;
//            }
//        });
//        webView.loadUrl(url);
    }

    private void smsPermissionCheck() {
        if (Build.VERSION.SDK_INT >= 23) {
            final ArrayList<String> permissionsNeeded = new ArrayList<>();

            if (checkSelfPermission(android.Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
                permissionsNeeded.add(android.Manifest.permission.RECEIVE_SMS);
            if (checkSelfPermission(android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED)
                permissionsNeeded.add(android.Manifest.permission.READ_SMS);

            Toast.makeText(this, String.valueOf(permissionsNeeded.size()), Toast.LENGTH_LONG).show();

            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), 100);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //checkNotificationAccess();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                Toast.makeText(this, "HAW", Toast.LENGTH_LONG).show();
                break;
            case 123:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    dial();
                else
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 123);
                break;
        }
    }
}
